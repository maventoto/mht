export default [
  {
    component: 'CNavTitle',
    name: 'Management',
  },
  {
    component: 'CNavItem',
    name: 'User',
    to: '/user',
    icon: 'cil-user',
  },
]

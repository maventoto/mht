import { h, resolveComponent } from 'vue'
import { createRouter, createWebHashHistory } from 'vue-router'

import DefaultLayout from '@/layouts/DefaultLayout'

const Login = () => import('@/views/pages/Login')

const User = () => import('@/views/management/user/Index')
const UserDetail = () => import('@/views/management/user/Detail')
const UserCreate = () => import('@/views/management/user/Create')
const UserEdit = () => import('@/views/management/user/Edit')

const routes = [
  {
    path: '/',
    meta: { needLogin: true },
    name: 'Home',
    component: DefaultLayout,
    redirect: '/user',
    children: [
      {
        path: '/user',
        meta: { needLogin: true },
        name: 'User',
        component: {
          render() {
            return h(resolveComponent('router-view'))
          },
        },
        redirect: '/user',
        children: [
          {
            path: '/user',
            meta: { needLogin: true },
            name: 'List User',
            component: User,
          },
          {
            path: '/user/:id',
            meta: { needLogin: true },
            name: 'Detail User',
            component: UserDetail,
            props: true,
          },
          {
            path: '/user/create',
            meta: { needLogin: true },
            name: 'Create user',
            component: UserCreate,
          },
          {
            path: '/user/edit/:id',
            meta: { needLogin: true },
            name: 'Edit User',
            component: UserEdit,
          },
        ],
      },
    ],
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
  },
]

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes,
  scrollBehavior() {
    // always scroll to top
    return { top: 0 }
  },
})

router.beforeEach((to, from, next) => {
  if (to.meta.needLogin) {
    let session = localStorage.getItem('token')

    if (session) {
      next()
    } else {
      next({
        path: '/login',
        query: {
          redirect: to.fullPath,
        },
      })
    }
  }
  next()
})

export default router
